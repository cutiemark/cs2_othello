This is the README file for the cs2_othello competition for CS2 for winter 2013 for the group cutie mark crusaders by Eric and Solomon.

Contributions:

Solomon Chang: I wrote the original minimax algorithm that we used then improved upon it by modifying it for alpha-beta.  Additionally, I rewrote the interations the board has with its representation to make it more efficient.


Eric Martin:
I wrote the interactions of our AI with the board (getting the next moves) and the various heuristics. Additionally I attempted to implement a transposition table and memoization but stopped after several hours due to an extremely bizarre bug (a line of code that was never executed was causing the program to hang). Also spent time learning about and prototyping a deep neural network for heuristic function, but then didn't implement due to difficulty of running external packages (namely scipy) on the Annenberg machines.

Improvements:

Overall, we feel that our AI will do reasonably well during the tournment.  However, due to time constraints we were unable to add majority of the features we originally planned for.  Our heruistic function is not just the simple count of rocks, but rather a weighted sum of multiple considerations given a board.  Our alpha-beta pruning algorithm allows us to search to a larger depth than the typical AI allowing us a distinct advantage.

The following features we tried to implement:
Memoization: However, the STL library had some weird interactions with the Java wrapper, after implementing the memoization, the Jave wrapper refused to interface with our class.  With the time constraints, the idea was dropped.
Iterative deeping: Time constraints cut this from our plans.
Deep neural network for heuristic: would prefer to use library, but couldn't figure out how to get library to work on cluster. Also would require communication between C++ and Python processes. Not much work was put into the existing heuristic because this was supposed to be the heuristic function.
