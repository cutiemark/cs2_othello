#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <list>
#include <utility>
#include <iostream>
#include "math.h"
#include "game_state.h"
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer;
typedef double (*Heuristic)(const GameState *state, const ExamplePlayer *player);

class ExamplePlayer {
public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();

    Move *doMove(Move *opponentsMove, int msLeft);
    Board current_board;
    Side my_side;

private:
    Move * minimax(Board *board, int depth, Heuristic heuristic);

    Move maxMove(GameState * game_state, int current_depth, int max_depth,
		      Heuristic heuristic, Side side_to_move);
    void minMove(GameState * game_state, int current_depth, int max_depth,
		      Heuristic heuristic, Side side_to_move);

    Move alpha_beta(GameState *game_state, int depth, double alpha,
                    double beta, Side side_to_move, Heuristic heuristic);

    std::list<GameState> get_next_boards(GameState *state, Side side);

    static double testHeuristic(const GameState *state, const ExamplePlayer *player){
        return 1.0;
    }

    static double corners(const GameState *state, const ExamplePlayer *player) {
        // its good to have corners

        Side my_side = player->my_side;
        Board board = state->board;
        double score = 0.0;

        list< pair<int, int> > corners;
        corners.push_back(pair<int, int>(0, 0));
        corners.push_back(pair<int, int>(0, 7));
        corners.push_back(pair<int, int>(7, 0));
        corners.push_back(pair<int, int>(7, 7));

        for (auto corner = corners.begin(); corner != corners.end(); corner++) {
            if (!board.occupied(corner->first, corner->second)) {
                continue;
            }
            if (board.get(my_side, corner->first, corner->second)) {
                score += 1.0;
            } else {
                score -= 1.0;
            }
        }
        return score;
    }

    static double immediate_mobility(const GameState *state, const ExamplePlayer *player) {
        // its good to be able to make a lot of different moves

        Side my_side = player->my_side;
        Side other_side = my_side == BLACK ? WHITE : BLACK;
        Board board = state->board;

        return board.getMoves(my_side).size() - board.getMoves(other_side).size();
    }

    static double in_x_squares(const GameState *state, const ExamplePlayer *player) {
        // don't want to be too close to corners
        Side my_side = player->my_side;
        Board board = state->board;
        double score = 0.0;

        list< pair<int, int> > x_squares;
        x_squares.push_back(pair<int, int>(1, 1));
        x_squares.push_back(pair<int, int>(6, 1));
        x_squares.push_back(pair<int, int>(1, 6));
        x_squares.push_back(pair<int, int>(6, 6));

        for (auto square = x_squares.begin(); square != x_squares.end(); square++) {
            if (!board.occupied(square->first, square->second)) {
                continue;
            }
            if (board.get(my_side, square->first, square->second)) {
                score -= 1.0;
            } else {
                score += 1.0;
            }
        }
        return score;
    }

    static double in_c_squares(const GameState *state, const ExamplePlayer *player) {
        // don't want to be too close to corners
        Side my_side = player->my_side;
        Board board = state->board;
        double score = 0.0;

        list< pair<int, int> > c_squares;
        c_squares.push_back(pair<int, int>(0, 1));
        c_squares.push_back(pair<int, int>(1, 0));

        c_squares.push_back(pair<int, int>(6, 0));
        c_squares.push_back(pair<int, int>(7, 1));

        c_squares.push_back(pair<int, int>(0, 6));
        c_squares.push_back(pair<int, int>(1, 7));

        c_squares.push_back(pair<int, int>(6, 7));
        c_squares.push_back(pair<int, int>(7, 6));

        for (auto square = c_squares.begin(); square != c_squares.end(); square++) {
            if (!board.occupied(square->first, square->second)) {
                continue;
            }
            if (board.get(my_side, square->first, square->second)) {
                score -= 1.0;
            } else {
                score += 1.0;
            }
        }
        return score;
    }

    static double num_stones(const GameState *state, const ExamplePlayer *player) {
        Side my_side = player->my_side;
        Side other_side = my_side == BLACK ? WHITE : BLACK;
        Board board = state->board;

        return board.count(my_side) - board.count(other_side);
    }

    static double heuristic(const GameState *state, const ExamplePlayer *player) {
        double score = 0.0;
        // use weights here to make linear combination

        score += 1.0 * corners(state, player);
        //score += 1.0 * immediate_mobility(state, player);
        score += 1.0 * in_x_squares(state, player);
        score += 1.0 * in_c_squares(state, player);
        score += 1.0 * num_stones(state, player);

        return score;
    }
};

#endif
