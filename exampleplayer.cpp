#include "exampleplayer.h"
#include "game_state.h"

#include "board.h"

#include <climits>
#include <cassert>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /*
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    my_side = side;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /*
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    current_board.doMove(opponentsMove, my_side == BLACK ? WHITE : BLACK);
    Move* my_move = minimax(&current_board, 8, ((Heuristic)heuristic));
    if(my_move){
        current_board.doMove(my_move, my_side);
    }
    return my_move;
}

/**
 * Computes the next move by traversing a game tree using the specified
 * heristic funciton.  This depth corresponds to a ply.
 *
 * Parameters: Board* board: A pointer to the the board that a move
 *                           is to be made one.
 *
 *             int depth:   The level which to search the game tree.
 *
 *             Heuristic heuristic: The heurisitc to evaluate them
 *                                  Game states
 *
 * Returns: Move, the move resulting from the minimax alogrithm.
 *
 */
Move* ExamplePlayer::minimax(Board *board, int depth, Heuristic heuristic){
    /*GameState starting(*board, Move(-1, -1));
    if (board->hasMoves(this->my_side)) {
        Move next_move = maxMove(&starting, 0, depth, heuristic, this->my_side);
        Move * result = new Move(next_move.getX(), next_move.getY());
        return result;
    } else {
        return NULL;
        }*/
    GameState starting(*board, Move(-1, -1));
    if (board->hasMoves(this->my_side)) {
        Move next_move = alpha_beta(&starting, depth, -100000, 100000,
                                    this->my_side, heuristic);
        Move * result = new Move(next_move.getX(), next_move.getY());
        return result;
    } else {
        return NULL;
    }
}

Move ExamplePlayer::maxMove(GameState * game_state, int current_depth,
                            int max_depth, Heuristic heuristic,
                            Side side_to_move){
    // If game is over or max depth, return self after evaluatation
    if(current_depth == max_depth || game_state->board.isDone()){
        game_state->h = (*heuristic)(game_state, this);
        return game_state->move;
    }

    // If I have no moves, then pass to next ply
    if(!game_state->board.hasMoves(side_to_move)){
        minMove(game_state, current_depth, max_depth, heuristic, getOtherSide(side_to_move));
        return Move(-1, -1);
    }

    std::list<GameState> next_states = get_next_boards(game_state, side_to_move);

    if(next_states.empty()){
        std::cout << "SOMEONE DUN FK UP" << std::endl;
        assert(1 == 0);
    }

    double max = -10000;
    Move best_move(0, 0);
    for(std::list<GameState>::iterator it = next_states.begin(); it != next_states.end(); ++it){
        minMove(&(*it), current_depth+1, max_depth, heuristic, getOtherSide(side_to_move));
        if(it->h > max){
            max = it->h;
            best_move = it->move;
        }
    }
    game_state->h = max;
    return best_move;
}

void ExamplePlayer::minMove(GameState * game_state, int current_depth,
                            int max_depth, Heuristic heuristic,
                            Side side_to_move){
    // If game is over or max depth, return self after evaluatation
    if(current_depth == max_depth || game_state->board.isDone()){
        game_state->h = (*heuristic)(game_state, this);
        return;
    }

    // If I have no moves, then pass to next ply, don't count as a depth
    if(!game_state->board.hasMoves(side_to_move)){
        maxMove(game_state, current_depth, max_depth, heuristic, getOtherSide(side_to_move));
        return;
    }

    std::list<GameState> next_states = get_next_boards(game_state, side_to_move);

    if(next_states.empty()){
        std::cout << "SOMEONE DUN FK UP" << std::endl;
        assert(1 == 0);
    }

    double min = 1000;
    for(std::list<GameState>::iterator it = next_states.begin(); it != next_states.end(); ++it){
        maxMove(&(*it), current_depth+1, max_depth, heuristic, getOtherSide(side_to_move));
        if(it->h < min){
            min = it->h;
        }
    }
    game_state->h = min;
    return;
}

std::list<GameState> ExamplePlayer::get_next_boards(GameState *state, Side side) {
    Board current = state->board;
    std::list<Move> moves = current.getMoves(side);

    std::list<GameState> states;
    for (auto move = moves.begin(); move != moves.end(); move++) {
        Board *next_board = current.copy();
        next_board->doMove(&(*move), side);

        GameState g = GameState(*next_board, *move);
        states.push_back(g);
    }
    return states;
}

Move ExamplePlayer::alpha_beta(GameState *game_state, int depth, double alpha,
                               double beta, Side side_to_move, Heuristic heuristic){
    // If game is over or max depth, return self after evaluation
    if(depth == 0 || game_state->board.isDone()){
        game_state->h = (*heuristic)(game_state, this);
        return game_state->move;
    }

    std::list<GameState> next_states = get_next_boards(game_state, side_to_move);
    Move best_move(-1, -1);

    if(side_to_move == my_side){// My move
        // If I have no moves, then pass to next ply
        if(next_states.empty()){
            alpha_beta(game_state, depth, alpha, beta,
                       getOtherSide(side_to_move), heuristic);
            game_state->h = max(game_state->h, alpha);
            return best_move;
        }

        for(std::list<GameState>::iterator it = next_states.begin();
            it != next_states.end();
            ++it){
            alpha_beta(&(*it), depth - 1, alpha, beta,
                       getOtherSide(side_to_move), heuristic);
            if(it->h > alpha){
                best_move = it->move;
                alpha = it->h;
            }
            if(beta <= alpha){
                break;
            }
        }
        game_state->h = alpha;
        return best_move;
    }else{// There move
        // If I have no moves, then pas to next ply
        if(next_states.empty()){
            alpha_beta(game_state, depth, alpha, beta,
                       getOtherSide(side_to_move), heuristic);
            game_state->h = min(game_state->h, beta);
            return best_move;
        }

        for(std::list<GameState>::iterator it = next_states.begin();
            it != next_states.end();
            ++it){
            alpha_beta(&(*it), depth - 1, alpha, beta,
                       getOtherSide(side_to_move), heuristic);
            beta = min(it->h, beta);
            if(beta <= alpha){
                break;
            }
        }
        game_state->h = beta;
        return best_move;
    }
}
