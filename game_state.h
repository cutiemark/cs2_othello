#ifndef __GAME_STATE_H__
#define __GAME_STATE_H__

#include "board.h"

const double DEFAULT_HEURISTIC = 1337.0;

struct GameState {
  Board board;
  Move move;
  double h;
  
  GameState(Board b, Move m):move(m.getX(), m.getY()) {
    board = b;
    h = DEFAULT_HEURISTIC;
  }

  bool has_h() {
    return h != DEFAULT_HEURISTIC;
  }
};

#endif
